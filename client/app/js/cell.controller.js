(function(){
    angular
        .module("InventoryApp")
        .controller("CellCtrl", CellCtrl);

    CellCtrl.$inject = ["$http", "$state", "AuthFactory", "InventoryService", "$rootScope"];

    function CellCtrl($http, $state, AuthFactory, InventoryService, $rootScope){
        var vm = this;
        vm.dateStart = null;
        vm.dateEnd = null;
        vm.inpputInvalid = false;
        vm.status = {
            message: "",
            code: 0
        };
        vm.data = [];
        vm.itemsPerPage = 20;
        vm.currentPage = 1;
        vm.totalPages = 0;
        vm.isUserLogon = false;
        vm.limit = 200;
        vm.cellChartsData=[];
        vm.chartsData = [];
        vm.chartDate = [];
        vm.chartAlarmData = [];
        vm.cell_id = 0;
        vm.cell = {};
        vm.title = "A01-CA0001";

        // function listAll(){
        //     console.log("--listAll()--");
        //     // var defer = $q.defer;
        //     $http
        //         .get("/api/dashboards/")
        //         .then(function(result){
        //             console.log("searched success");
        //             vm.data = result.data;
        //         })
        //         .catch(function(err){
        //             console.log("search failed");
        //         });
        //     // return defer.promise;
        // }

        vm.init=function(){
            vm.isUserLogon = AuthFactory.isLoggedIn();
            if(!vm.isUserLogon){
                $rootScope.pageStatus = 4;
                $state.go("login");
                return;
            }
            if(InventoryService.cell_id){
                vm.cell_id = InventoryService.cell_id;
            }else{
                vm.cell_id = 1;
            }
            vm.dateEnd = new Date();
            vm.dateStart = new Date(new Date() - 30 * 24 * 60 * 60 * 1000);
            getCellChartData(vm.cell_id, vm.limit, formDateString(0), formDateString(1));
            $rootScope.pageStatus = 2;
        };

        function formDateString(option){
            var timeString = "";
            var d = null;
            if(option == 0){
                d = vm.dateStart;
                timeString = " 00:00:00";
            }else{
                d = vm.dateEnd;
                timeString = " 59:59:59";
            }
            var month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('-') + timeString;
        }

        vm.submit = function () {
            var dateStart = formDateString(0);
            var dateEnd = formDateString(1);
            console.log("--submit()--" + dateStart + "--" + dateEnd);
            getCellChartData(vm.cell_id, vm.limit, dateStart, dateEnd);

        };
        vm.showPreviousChart = function(){
            if(vm.cell_id == 1){
                // reached the first cell do nothing
                return;
            }
            vm.cell_id--;
            getCellChartData(vm.cell_id, vm.limit, formDateString(0), formDateString(1));
        };

        vm.showNextChart = function(){
            if(vm.cell_id == 20){
                // reached the first cell do nothing
                return;
            }
            vm.cell_id++;
            getCellChartData(vm.cell_id, vm.limit, formDateString(0), formDateString(1));
        };

        function getCellChartData(cell_id, limit, dateStart, dateEnd){
            console.log("--getCellChartData--" + cell_id + "--" + limit + "--" + dateStart + "--" + dateEnd);
            InventoryService.getCells("cell_id", cell_id)
                .then(function (success) {
                    vm.cell = InventoryService.cellData;
                    console.log("--cell_controller_init--getCells--", vm.cell);
                    InventoryService.getCellChartData(cell_id, limit, dateStart, dateEnd)
                        .then(function (success1) {
                            vm.cellChartData = InventoryService.cellChartData;
                            $rootScope.pageStatus = 2;
                            formChartData();
                        }, function (error1) {
                            console.log("--cell_controller_init--getCellChartData failed", error1);
                        })
                        .catch(function (err1) {
                            console.log("--cell_controller_init--getCellChartData servo error", err1);
                        });
                }, function(error){
                    console.log("--cell_controller_init--getCells error", error);
                })
                .catch(function(err){
                    console.log("--cell_controller_init--getCells servo error", err);
                });
        }

        function formChartData(){
            console.log("--formChartData--", vm.cellChartData);
            vm.chartData = [];
            vm.chartDate = [];
            var length = vm.cellChartData.length;
            var chartAlarmData = new Array(length);
            var alarmWeight = Number(vm.cell[0].alarm_weight);
            var fullWeightData = new Array(length);
            var fullWeight = Number(vm.cell[0].full_weight);
            if(length){
                for(var i = 0; i < length; i++){
                    vm.chartData.push(vm.cellChartData[i].weight);
                    chartAlarmData[i] = alarmWeight;
                    fullWeightData[i] = fullWeight;
                    // xAixis date
                    var d = vm.cellChartData[i].createdAt.substring(0,10) + ":" + vm.cellChartData[i].createdAt.substring(11,19);
                    vm.chartDate.push(d);
                }
            }
            // vm.chartConfig.series[0].data = [1,2,3,4,5,6,7+vm.cell_id,8,9,10,1,2,3];
            // vm.chartConfig.series[1].data = [1.5,1.5,1.5,1.5,1.5+vm.cell_id,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5];
            vm.chartConfig.series[0].data = vm.chartData;
            vm.chartConfig.series[1].data = chartAlarmData;
            vm.chartConfig.series[2].data = fullWeightData;
            vm.chartConfig.title.text = vm.cell[0].node_address + "-" + vm.cell[0].cell_address;
            vm.chartConfig.xAxis.categories = vm.chartDate;
            vm.chartConfig.xAxis.currentMax = vm.chartDate.length - 1;
            vm.chartConfig.yAxis.currentMax = vm.cell[0].full_weight + 5;
            console.log(vm.chartData);
            // console.log(vm.chartDate);
            console.log(chartAlarmData);
        }

        //This is not a highcharts object. It just looks a little like one!
        vm.chartConfig = {

            options: {
                //This is the Main Highcharts chart config. Any Highchart options are valid here.
                //will be overriden by values specified below.
                chart: {
                    type: 'line'
                },
                "plotOptions": {
                    "series": {
                        "stacking": "",
                    }
                }
            },
            //The below properties are watched separately for changes.

            //Series object (optional) - a list of series using normal Highcharts series options.
            series: [{
                name: "Weight",
                data: []
            }, {
                name: "Triggering weight",
                color: '#ff0000',
                data: []
            },{
                name: "Full weight",
                color: '#00ff00',
                data: []
            }],
            //Title configuration (optional)
            title: {
                text: ""
                // text: vm.cell.node_address + "-" + vm.cell.cell_address
            },
            //Boolean to control showing loading status on chart (optional)
            //Could be a string if you want to show specific loading text.
            loading: false,
            //Configuration for the xAxis (optional). Currently only one x axis can be dynamically controlled.
            //properties currentMin and currentMax provided 2-way binding to the chart's maximum and minimum
            // xAxis: {
            //     currentMin: 0,
            //     currentMax: 20,
            //     title: {text: ''},
            //     credits: {
            //         enabled: true
            //     }
            // },
            xAxis: {
                currentMin: 0,
                currentMax: 20,
                categories:  [],
                title: {text: 'Date (yyyy-mm-dd:hh:mm:ss)'}
            },
            yAxis: {
                currentMin: 0,
                currentMax: 15,
                title: {text: 'Weight(kg)'},
                credits: {
                    enabled: true
                }
            },
            //Whether to use Highstocks instead of Highcharts (optional). Defaults to false.
            useHighStocks: false,
            //size (optional) if left out the chart will default to size of the div or something sensible.
            size: {
                width: 800,
                height: 600
            },
            //function (optional)
            func: function (chart) {
                //setup some logic for the chart
            }
        };

    }
})();