(function(){
    angular
        .module("InventoryApp")
        .controller("PartsManagerCtrl", PartsManagerCtrl);

    PartsManagerCtrl.$inject = ["$state", "AuthFactory", "InventoryService"];
    function PartsManagerCtrl($state, AuthFactory, InventoryService){
        var vm = this;

        vm.isUserLogon = false;
        vm.editEn = false;
        vm.addEn = false;
        vm.confirmUpdate = false;
        vm.part = {};
        vm.parts = [];

        vm.pm_init = function(){
            vm.isUserLogon = AuthFactory.isLoggedIn();
            if(vm.isUserLogon){
                console.log("--pm_init--");
                vm.pageStatus = 7;
                InventoryService.getPartsList(null, null)
                    .then(function(success){
                        console.log("--pm_init--getPartsList succeeded");
                        vm.parts = InventoryService.partsData;
                    }, function (error) {
                        console.log("--pm_init--getPartsList failed", error);
                    })
                    .catch(function(err){
                        console.log("--pm_init--getPartsList server error", err);
                    })

            }else{
                $state.go("login");
            }
        };

        // for dashboard Moving searching form to nav bar, but not working
        vm.searchKey = "";
        vm.searchGroupList = [
            {id: "1", name: "All"},
            {id: "2", name: "By Part Number"},
            {id: "3", name: "By Part Name"},
            {id: "4", name: "By Mfc Part Name"},
            {id: "5", name: "By Vendor ID"},
            {id: "6", name: "By Vendor Part Number"},
            {id: "7", name: "By Purchaser ID"}
        ];
        vm.searchGroup = "2";
        vm.submit = function () {
            InventoryService.getPartsList(vm.searchGroup, vm.searchKey)
                .then(function(success){
                    console.log("--pm_submit--getPartsList succeeded");
                    vm.parts = InventoryService.partsData;
                }, function (error) {
                    console.log("--pm_submit--getPartsList failed", error);
                })
                .catch(function(err){
                    console.log("--pm_submit--getPartsList server error", err);
                });
        };

        vm.pm_update = function () {
            // just set flag to enable confirm message and button
            // actual update operation is done by submit function
            vm.confirmUpdate = true;
            console.log("--cellsmanager.controller--update--vm.node", vm.node);
        };
        vm.pm_cancel = function () {
            vm.editEn = false;
            vm.confirmUpdate = false;
            vm.addEn = false;
        };
        vm.pm_confirmNo = function () {
            vm.confirmUpdate = false
        };
        function getPartFromArray(partID){
            var id = Number(partID);
            for (var i = 0; i < vm.parts.length; i++){
                if(id === vm.parts[i].id){
                    return vm.parts[i];
                }
            }
            return {};
        }
        vm.pm_editPart = function (partID) {
            console.log("--vm.pm_editPart--", partID);
            vm.editEn = true;
            vm.confirmUpdate = false;
            vm.part = getPartFromArray(partID);
            console.log("--vm.pm_editPart--", vm.part);
        };
        vm.pm_updateSubmit = function () {
            console.log("--vm.pm_updateSubmit--");
            vm.editEn = false;
            vm.confirmUpdate = false;
            InventoryService.updatePart(vm.part)
                .then(function(success){
                    console.log("--pm_updateSubmit--updatePart--succeeded");
                }, function (error) {
                    console.log("--pm_updateSubmit--updatePart failed", error);
                })
                .catch(function(err){
                    console.log("--pm_updateSubmit--updatePart server error", err);
                });

            console.log("--vm.pm_editPart--", vm.part);
        };
        vm.pm_addPartEn = function(){
            vm.addEn = true;
            vm.part = {};
        };
        vm.pm_addPart = function(){
            vm.addEn = false;
            InventoryService.addParts(vm.part)
                .then(function(success){
                    console.log("--pm_addPart--succeeded");
                }, function (error) {
                    console.log("--pm_addPart failed", error);
                })
                .catch(function(err){
                    console.log("--pm_addPart server error", err);
                });
        }

    }
})();
