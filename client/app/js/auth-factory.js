(function () {

angular
    .module('InventoryApp')
    .factory('AuthFactory',
    ['$q', '$timeout', '$http', 'Flash', '$state',
        function ($q, $timeout, $http, Flash, $state) {

            // create user variable
            var user = null;
            var email = null;
            // return available functions for use in the controllers
            return ({
                isLoggedIn: isLoggedIn,
                getUserStatus: getUserStatus,
                login: login,
                logout: logout,
                register: register,
                isAdmin: isAdmin,
                getEmployeeId: getEmployeeId,
                getEmail: getEmail
            });

            function isLoggedIn() {
                console.log("is Loggedin " + JSON.stringify(user));
                if(user) {
                    return true;
                } else {
                    return false;
                }
            }

            function isAdmin() {
                if(user){
                    return true;
                }else {
                    return false;
                }
            }

            function getEmployeeId() {
                if(user){
                    return "1001";
                }
            }

            function getEmail() {
                if(email){
                    return email;
                }
            }

            function getUserStatus(callback) {
                $http.get('/status/user')
                // handle success
                    .then(function (data) {
                        var authResult = JSON.stringify(data);
                        if(data["data"] != ''){
                            user = true;
                            console.log("getUserStatus-->", data);
                            email = data.data;
                            console.log("email-->", email);
                            callback(user);
                        } else {
                            user = false;
                            callback(user);
                        }
                    });
            }

            function login(userProfile) {

                // create a new instance of deferred
                var deferred = $q.defer();

                // send a post request to the server
                $http.post('/login',
                    userProfile)
                // handle success
                    .success(function (data, status) {
                        // console.log("login---status:"+status + "--data", data);
                        if(status == 200){
                            getUserStatus(function(result){
                                if(result){
                                    deferred.resolve();
                                    $state.go('home');
                                }else{
                                    deferred.reject();
                                    Flash.create('danger', "Ooops having issue logging in!", 0, {class: 'custom-class', id: 'custom-id'}, true);
                                    $state.go('login');
                                }
                            });
                        } else {
                            user = false;
                            Flash.clear();
                            Flash.create('danger', "Ooops having issue logging in!", 0, {class: 'custom-class', id: 'custom-id'}, true);
                            deferred.reject();
                        }
                    })
                    // handle error
                    .error(function (data) {
                        // console.log("login--error--data", data);
                        user = false;
                        Flash.clear();
                        Flash.create('danger', "Ooops having issue logging in!", 0, {class: 'custom-class', id: 'custom-id'}, true);
                        deferred.reject();
                    });

                // return promise object
                return deferred.promise;

            }

            function logout() {

                // create a new instance of deferred
                var deferred = $q.defer();

                // send a get request to the server
                $http.get('/logout')
                // handle success
                    .success(function (data) {
                        user = false;
                        deferred.resolve();
                    })
                    // handle error
                    .error(function (data) {
                        user = false;
                        deferred.reject();
                    });

                // return promise object
                return deferred.promise;

            }

            function register(user) {

                // create a new instance of deferred
                var deferred = $q.defer();

                // send a post request to the server
                $http.post('/api/users/', user)
                // handle success
                    .success(function (data, status) {
                        if(status){
                            deferred.resolve();
                        } else {
                            Flash.closeFlash();
                            Flash.create('danger', "Can't register with us!", 0, {class: 'custom-class', id: 'custom-id'}, true);
                            deferred.reject();
                        }
                    })
                    // handle error
                    .error(function (data) {
                        Flash.clear();
                        Flash.create('danger', "Ooops something went wrong!", 0, {class: 'custom-class', id: 'custom-id'}, true);
                        deferred.reject();
                    });

                // return promise object
                return deferred.promise;

            }

        }]);
})();