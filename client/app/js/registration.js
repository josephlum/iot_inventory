/**
 * Created by LENOVO on 10/24/2016.
 */
(function(){
    var RegApp = angular.module("RegApp", []);
    var RegCtrl = function($http) {
        var regCtrl = this;

        regCtrl.form = {
            firstname: "Your first name",
            lastname: "Your last name",
            employeeid: "Employee ID",
            email: "Your_email@company.com",
            password: "",
            contact: "",
            usertype: "User"
        };

        regCtrl.resetEntry = function() {
            console.info("Reset Entry");
            regCtrl.form.firstname = "";
            regCtrl.form.lastname = "";
            regCtrl.form.employeeid = "";
            regCtrl.form.email = "";
            regCtrl.form.password = "";
            regCtrl.form.contact = "";
            regCtrl.form.usertype = "User";
        };

        regCtrl.sendSuccess = false;

        regCtrl.submit = function() {
            /*
            $http
                .post("thankyou.html",regCtrl.form)
                //.post("/api/users/123456789?test=true&mock=true",{firstName: "Jon", lastName: "Snow"})
                .then(function (response) {

                    // if you want to redirect
                    //window.location.href = response.data.redirectUrl;

                    // Display message on the same page
                    regCtrl.sendSuccess = true;

                    console.log("Form Submitted");
                })
                .catch(function (response) {
                    // if you want to redirect
                    //window.location.href = response.data.redirectUrl;

                    // Display message on the same page
                    regCtrl.sendSuccess = false;

                    console.log("Form submission failed");
                }); */
            regCtrl.sendSuccess = true;  // for test only */
            if (regCtrl.sendSuccess){
                $http
                    .get("thaq.html")
                    .then(function(response){
                        regCtrl.data = response.data;
                        regCtrl.status = response.status;
                        regCtrl.statusText = response.statusText;
                    })
                    .catch(function(response){
                        regCtrl.data = response.data;
                        regCtrl.status = response.status;
                        regCtrl.statusText = response.statusText;
                    });
            }

            console.info("Submit");
            console.info(regCtrl.form);
            console.info(regCtrl.data);
            console.info(regCtrl.status);
            console.info(regCtrl.statusText);

        };


    };

    RegApp.controller("RegCtrl", ["$http", RegCtrl]);

})();
