(function(){
    angular
        .module("InventoryApp")
        .controller("DashboardCtrl", DashboardCtrl);

    DashboardCtrl.$inject = ["$http", "$state", "$q", "AuthFactory", "InventoryService"];
    function DashboardCtrl($http, $state, $q, AuthFactory, InventoryService){
        var vm = this;
        vm.cellcounts = 20;
        vm.columns = 10;
        vm.searchKey = "";
        vm.searchGroupList = [
            {id: "1", name: "All"},
            {id: "2", name: "By Node Address"},
            {id: "3", name: "By Cell Address"},
            {id: "4", name: "By Part Name"},
            {id: "5", name: "By Part Number"},
            {id: "6", name: "By Mfc Part Name"}
        ];
        vm.searchGroup = "1";
        vm.username = "User Name";
        vm.logout = {
            model: "",
            options: [vm.username, "Log out"]
        };
        vm.data = [];
        vm.data_2d = [];
        vm.inventory_monitors = [];
        vm.triggered_data = [];
        vm.triggered_data_2d = [];
        // vm.cells stores all cells data only initialized at beginning
        vm.cells = [];
        vm.itemsPerPage = 20;
        vm.currentPage = 1;
        vm.totalPages = 0;

        vm.init = function(){
            vm.isUserLogon = AuthFactory.isLoggedIn();
            if (vm.isUserLogon){
                InventoryService.getCells(null, null)
                    .then(function(success){
                        vm.data = InventoryService.cellData;
                        vm.cellcounts = vm.data.length;
                        vm.cells = vm.data;
                        InventoryService.getInventoryWeight({limit: vm.cellcounts})
                            .then(function(success1){
                                console.log("--dashboard.controller--init:sucess");
                                vm.inventory_monitors = InventoryService.inventory_monitors;
                                addWeightToCell();
                                rearangeData();
                            }, function(err){
                                console.log("--dashboard.controller--init:err", err);
                            });
                    });
            }else{
                $state.go("login");
            }
        };

        function rearangeData(){
            while (vm.data.length){
                if (vm.data.length >= vm.columns){
                    vm.data_2d.push(vm.data.splice(0, vm.columns));
                }else{
                    vm.data_2d.push(vm.data.splice(0, vm.data.length));
                }
            }
            while (vm.triggered_data.length){
                if (vm.triggered_data.length >= vm.columns){
                    vm.triggered_data_2d.push(vm.triggered_data.splice(0, vm.columns));
                }else{
                    vm.triggered_data_2d.push(vm.triggered_data.splice(0, vm.triggered_data.length));
                }
            }
            console.log("vm.data_2d", vm.data_2d);
            console.log("vm.triggered_data_2d", vm.triggered_data_2d);
        }

        function addWeightToCell(){
            console.log("vm_data", vm.data);
            console.log("vm_cells", vm.cells);
            var length = vm.data.length;
            var i = 0;
            for(i = 0; i < vm.cellcounts; i ++){
                var weight = vm.inventory_monitors[i].weight * 100 / vm.cells[i].full_weight;
                vm.cells[i].weight = weight.toFixed(2);
                if (weight <= (vm.cells[i].alarm_weight * 100 / vm.cells[i].full_weight)){
                    vm.triggered_data.push(vm.cells[i]);
                }
            }
            for(i = 0; i < length; i ++){
                var id = Number(vm.data[i].id);
                var weight = vm.inventory_monitors[id-1].weight * 100 / vm.data[i].full_weight;
                vm.data[i].weight = weight.toFixed(2);
            }
        }

        vm.showCell = function(cellId){
            console.log("--showCell--"+cellId);
            InventoryService.setCellId(cellId);
            $state.go("cell");
        };

        vm.showTriggeredCell = function(cellId){
            var limit = 20;
            $http
                .get("/api/inventory_monitors", {params: {cell_id: cellId, limit: limit}})
                .then(function(result){
                    console.log("searched success");
                    vm.inventory_monitors = result.data;
                    console.log("vm.inventory_monitors", vm.inventory_monitors);

                })
                .catch(function(err){
                    console.log("search failed");
                });
        };

        vm.submit = function () {
            console.log("--submit()--" + vm.searchGroup + "--" + vm.searchKey);
            vm.data_2d=[];
            vm.triggered_data_2d=[];
            vm.isUserLogon = AuthFactory.isLoggedIn();
            if (vm.isUserLogon){
                InventoryService.getCells(null, null)
                    .then(function(success1){
                        // unknown issue: vm.cells was cleared, why?
                        // temp solution: re-fetch
                        vm.cells = InventoryService.cellData;
                        InventoryService.getCells(vm.searchGroup, vm.searchKey)
                            .then(function(success2) {
                                vm.data = InventoryService.cellData;

                                InventoryService.getInventoryWeight({limit: vm.cellcounts})
                                    .then(function (success3) {
                                        console.log("--dashboard.controller--submit:sucess");
                                        vm.inventory_monitors = InventoryService.inventory_monitors;
                                        addWeightToCell();
                                        rearangeData();
                                    }, function (err1) {
                                        console.log("--dashboard.controller--submit--err1:", err1);
                                    });
                            }, function (err2){
                                console.log("--dashboard.controller--submit--err2:", err2);
                            })
                    }, function(err3){
                        console.log("--dashboard.controller--submit--err2:", err2);
                    });
            }

        };
        // if (!vm.data_2d){
        //     vm.submit();
        // }
        //
        // if(InventoryService.submitted){
        //     vm.searchGroup = InventoryService.searchGroup;
        //     vm.searchKey = InventoryService.searchKey;
        //     InventoryService.clearDashboardSubmitFlag();
        //     vm.submit();
        //
        // }

    }
})();