(function(){
    angular
        .module("InventoryApp")
        .config(InventoryAppConfig);

    InventoryAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];

    function InventoryAppConfig($stateProvider, $urlRouterProvider){
        $stateProvider
            .state("home", {
                url: "/home",
                views: {
                    "inventory": {
                        templateUrl: "/app/dashboard/dashboard.html",
                        controller: "DashboardCtrl",
                        controllerAs: "ctrl"
                    }
                }
            })
            .state("/", {
                url: "/",
                views: {
                    "inventory": {
                        templateUrl: "/app/dashboard/dashboard.html",
                        controller: "InventoryCtrl",
                        controllerAs: "ctrl"
                    }
                }
            })
            .state("login", {
                url: "/login",
                views:  {
                    "inventory": {
                        templateUrl: "/app/users/login.html",
                        controller: "LoginCtrl",
                        controllerAs: "ctrl"
                    }
                }
            })
            .state("logout", {
                url: "/logout",
                views:  {
                    "inventory": {
                        templateUrl: "/app/users/logout.html",
                        controller: "LoginCtrl",
                        controllerAs: "ctrl"
                    }
                }
            })
            .state("register", {
                url: "/register",
                views:  {
                    "inventory": {
                        templateUrl: "/app/users/register.html",
                        controller: "RegisterCtrl",
                        controllerAs: "ctrl"
                    }
                }
            })
            .state("cell", {
                url: "/cell",
                views:  {
                    "inventory": {
                        templateUrl: "/app/cell/cell.html",
                        controller: "CellCtrl",
                        controllerAs: "ctrl"
                    }
                }
            })
            .state("cellsmanager", {
                url: "/cellsmanager",
                views:  {
                    "inventory": {
                        templateUrl: "/app/cell/cells_manager.html",
                        controller: "CellsManagerCtrl",
                        controllerAs: "ctrl"
                    }
                }
            })
            .state("partsmanager", {
                url: "/partsmanager",
                views:  {
                    "inventory": {
                        templateUrl: "/app/part/parts_manager.html",
                        controller: "CellsManagerCtrl",
                        controllerAs: "ctrl"
                    }
                }
            })
            .state("setting", {
                url: "/setting",
                views:  {
                    "inventory":{
                        templateUrl: "/app/setting/setting.html",
                        controller: "SettingCtrl",
                        controllerAs: "ctrl"
                    }
                }
            })
            .state("import", {
                url: "/import",
                views:  {
                    "inventory":{
                        templateUrl: "/app/tools/import.html",
                        controller: "ImportCtrl",
                        controllerAs: "ctrl"
                    }
                }
            })
            .state("export", {
                url: "/export",
                views:  {
                    "inventory":{
                        templateUrl: "/app/tools/export.html",
                        controller: "ExportCtrl",
                        controllerAs: "ctrl"
                    }
                }
            })
            .state("addcell", {
                url: "/addcell",
                views:  {
                    "inventory":{
                        templateUrl: "/app/cell/addcell.html",
                        controller: "CellsManagerCtrl",
                        controllerAs: "ctrl"
                    }
                }
            });
        $urlRouterProvider.otherwise("/login");
    }
})();
