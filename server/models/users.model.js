var Sequelize = require("sequelize");

module.exports = function (database) {

    var Users = database.define("users", {

        id: {
            type: Sequelize.INTEGER.UNSIGNED,
            autoIncrement: true,
            primaryKey: true
        },
        employee_id: {
            type: Sequelize.STRING,
            allowNull: false
        },
        first_name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        last_name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        email: {
            type: Sequelize.STRING,
            allowNull: false
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false
        },
        phone: {
            type: Sequelize.STRING,
            allowNull: true
        },
        access_group: {
            type: Sequelize.ENUM('User', 'Purchaser', 'Admin'),
            defaultValue: "User",
            allowNull: false
        }
    },{
        freezeTableName: true,
        timestamps:true
    });

    return Users;

};