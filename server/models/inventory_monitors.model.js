var Sequelize = require("sequelize");
var Cells = require("./cells.model");

// cellAddr – String, Primary Key
// quantity - Float
// weight – Float

module.exports = function (database) {

    var Inventory_monitors = database.define("inventory_monitors", {

        id: {
            type: Sequelize.INTEGER.UNSIGNED,
            autoIncrement: true,
            primaryKey: true
        },
        cell_id: {
            type: Sequelize.INTEGER.UNSIGNED,
            reference: "cells",
            referenceKey: "id",
            onUpdate: "casecade",
            allowNull: false
        },
        quantity: {
            type: Sequelize.FLOAT,
            allowNull: true
        },
        weight: {
            type: Sequelize.FLOAT,
            allowNull: false
        }
    },{
        freezeTableName: true,
        timestamps:true
    });

    return Inventory_monitors;

};