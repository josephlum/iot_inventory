var Sequelize = require("sequelize");
var Vendors = require("./vendors.model");
var Users = require("./users.model");


module.exports = function (database) {

    var Parts = database.define("parts", {

        id: {
            type: Sequelize.INTEGER.UNSIGNED,
            autoIncrement: true,
            primaryKey: true
        },
        part_number: {
            type: Sequelize.STRING,
            allowNull: false
        },
        part_name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        mfc_part_number: {
            type: Sequelize.STRING,
            allowNull: false
        },
        unit: {
            type: Sequelize.STRING,
            allowNull: false
        },
        unit_price: {
            type: Sequelize.FLOAT,
            allowNull: false
        },
        unit_weight: {
            type: Sequelize.FLOAT,
            allowNull: false
        },
        vendor_id: {
            type: Sequelize.INTEGER.UNSIGNED,
            reference: "vendors",
            referenceKey: "id",
            onUpdate: "casecade",
            allowNull: false
        },
        vendor_part_number: {
            type: Sequelize.STRING,
            allowNull: false
        },
        purchaser_id: {
            type: Sequelize.INTEGER.UNSIGNED,
            reference: "users",
            referenceKey: "id",
            onUpdate: "casecade",
            allowNull: false
        }
    },{
        freezeTableName: true,
        timestamps:true
    });

    return Parts;

};