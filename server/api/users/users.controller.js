var Users = require("../../database").User;
var bcrypt   = require('bcryptjs');

console.log("---Users", Users);
exports.list = function(req,res){
    console.log("reached users database >> list");
    var whereCondition = {};
    var listLimit = 20;
    if (query.username !== undefined){
        if (query.username.search("@") > 0) {
            // email
            whereCondition = {email: query.username};
        }else{
            // employee_id
            whereCondition = {employee_id: query.username};
        }
    }else if(query.accessGroup !== undefined){
        whereCondition = {access_group: req.query.accessGroup}
    }
    if(query.limit !== undefined){
        listLimit = query.limit;
    }
    console.log("whereCondition", whereCondition);
    Users
        .findAll({
            where: whereCondition,
            limit: listLimit
        })
        .then(function (result) {
            if (result) {
                console.log(result);
                res.json(result);
            } else {
                // search failed, then search by first name or last name
                Users
                    .findAll({
                        where: {
                            $or: [{
                                first_name: {
                                    $like: "%" + req.query.username + "%"
                                }
                            }, {
                                last_name: {
                                    $like: "%" + req.query.username + "%"

                                }
                            }]
                        },
                        limit: listLimit
                    })
                    .then(function (result) {
                        if (result) {
                            res.json(result);
                        } else {
                            res.status(400).send(JSON.stringify("Record Not Found"));
                        }
                    });
            }
        });
};

exports.delete = function(req,res){
    console.log("--reached users database--delete");
    console.log(req.params);
    Users
        .destroy({
            where: {
                id: req.params.userId
            }
        })
        .then(function(result) {
            console.log("deleted");
            res
                .status(200)
                .json(result)
        })
        .catch(function(err){
            console.log("err", err);
            res
                .status(500)
                .json({error: true})
        })

};

exports.update = function(req,res) {
    console.log("--reached users database--update");
    console.log(req.params);
    var hashpassword = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);
    Users
        .find({
            where: {
                id: Number(req.params.userId)
            }
        })
        .then(function (response) {
            console.log('DB Response', response);
            response.updateAttributes({
                employee_id: req.body.employee_id,
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                email: req.body.email,
                password: hashpassword,
                phone: req.body.phone,
                access_group: req.body.access_group
            }).then(function () {
                console.log("update done");
                res.status(200).end();
            }).catch(function () {
                console.log("update failed");
                res
                    .status(500)
                    .json({error: true, errorText: "Update Failed"})
            });
        })
        .catch(function (err) {
            console.log("err", err);
            res
                .status(500)
                .json({error: true, errorText: "Record not found"})
        });
};

exports.add = function(req,res){
    console.log("--reached users database--add");
    console.log(req.body);
    var hashpassword = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);
    Users
        .create({
            employee_id: req.body.employee_id,
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email: req.body.email,
            password: hashpassword,
            phone: req.body.phone,
            access_group: req.body.access_group
        })
        .then(function(user){
            console.log("User added");
            res.status(201).json(user);
        })
        .catch(function(err){
            console.log("err", err);
            res.status(409)
               .json({error: true, errorText: "Add user failed"})
        });
};
