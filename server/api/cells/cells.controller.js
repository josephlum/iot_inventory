var Cells = require("../../database").Cell;

console.log("---Cells", Cells);
function searchCriteria(query){
    var where = {};
    if (query.key !== undefined){
        if (query.searchOption !== undefined){
            switch (query.searchOption){
                case "1":
                    // All
                    where = {
                        $or: [
                            {
                                node_address: {
                                    $like: "%" + query.key + "%"
                                }
                            },
                            {
                                cell_address: {
                                    $like: "%" + query.key + "%"
                                }
                            }
                        ]
                    };
                case "2":
                    // By Node Address
                    where = {
                        node_address: {
                            $like: "%" + query.key + "%"
                        }
                    };
                    break;
                case "3":
                    // By Cell Address
                    where = {
                        cell_address: {
                            $like: "%" + query.key + "%"
                        }
                    };
                    break;
                case "4":
                    // By Part Name
                    // where = {
                    //     cell_address: {
                    //         $like: "%" + params.key + "%"
                    //     }
                    // };
                    break;
                case "5":
                    // By Part Number
                    // where = {
                    //     cell_address: {
                    //         $like: "%" + params.key + "%"
                    //     }
                    // };
                    break;
                case "6":
                    // By Mfc Part Name
                    // where = {
                    //     cell_address: {
                    //         $like: "%" + params.key + "%"
                    //     }
                    // };
                    break;
                default:
                    break;

            }
        }
    }
    console.log("where", where);
    return where;
}
exports.list = function(req,res){
    console.log("reached cells database >> list");
    console.log("req.query", req.query);
    var where = {};
    var limit = 60;
    if('cell_id' == req.query.searchOption){
        // single cell query
        where = {id: Number(req.query.key)};
        limit = 1;
    }else{
        var where = searchCriteria(req.query);
    }
    console.log(where);
    Cells
        .findAll({
            where: where,
            limit: limit
        })
        .then(function (result) {
            if (result) {
                // console.log(result);
                res.json(result);
            } else {
                res.status(400).send(JSON.stringify("Record Not Found"));
            }
        });
};

exports.delete = function(req,res){
    console.log("--reached cells database--delete");
    console.log(req.params);
    Cells
        .destroy({
            where: {
                id: req.params.cellId
            }
        })
        .then(function(result) {
            console.log("deleted");
            res
                .status(200)
                .json(result)
        })
        .catch(function(err){
            console.log("err", err);
            res
                .status(500)
                .json({error: true})
        })

};

exports.update = function(req,res) {
    console.log("--reached cells database--update");
    console.log(req.params);
    Cells
        .find({
            where: {
                id: Number(req.params.cellId)
            }
        })
        .then(function (response) {
            console.log('DB Response', response);
            response.updateAttributes({
                node_address: req.body.node_address,
                cell_address: req.body.cell_address,
                part_id: req.body.part_id,
                full_weight: req.body.full_weight,
                min_weight: req.body.min_weight,
                alarm_weight: req.body.alarm_weight
            }).then(function () {
                console.log("update done");
                res.status(200).end();
            }).catch(function () {
                console.log("update failed");
                res
                    .status(500)
                    .json({error: true, errorText: "Update Failed"})
            });
        })
        .catch(function (err) {
            console.log("err", err);
            res
                .status(500)
                .json({error: true, errorText: "Record not found"})
        });
};

function mysqlUpdate(i, iMax, cells){
    if (i < iMax){
        Cells
            .update(
                {cell_address: cells[i].cell_address},{where:{id: cells[i].id}}
            )
            .then(function(){
                console.log("update id=" + req.body[i].id + " done");
                res.status(200).end();
                // mysqlUpdate(i++, iMax, cells);
            })
            .catch(function(err){
                console.log("update id=" + req.body[i].id + " failed", err);
                errFlag = true;
            });
    }
}
exports.bulkUpdate = function(req, res){
    var errorFlag = true;
    req.body.forEach(function(cell){
        Cells
            .update({
                    node_address: cell.node_address,
                    part_id: cell.part_id,
                    full_weight: cell.full_weight,
                    min_weight: cell.min_weight,
                    alarm_weight: cell.alarm_weight
            },{
                where: {id: cell.id}
            })
            .then(function(){
                console.log("update id=" + cell.id + " done");
                errorFlag = false;
            })
            .catch(function(err){
                console.log("update id=" + cell.id + " failed", err);
                errorFlag = true;
            });
    });
    if(errorFlag){
        res
            .status(500)
            .json({error: true, errorText: "Update Failed"});
    }else{
            res.status(200).end();
    }
};
// exports.bulkUpdate = function(req,res) {
//     console.log("--reached cells database--bulkUpdate");
// //    console.log(req.body);
// //    res.status(200).end();
//     // req.body is an array of objects
//     var length = req.body.length;
//     // var updataData = [];
//     var nodeAddr = [];
//     var cellAddr = [];
//     var idArray = [];
//     var i = 0;
//     for ( i = 0; i < length; i++){
//         nodeAddr.push(req.body[i].node_address);
//         cellAddr.push(req.body[i].cell_address);
//         idArray.push(Number(req.body[i].id));
//     }
//     console.log("nodeAddr", nodeAddr);
//     console.log("cellAddr", cellAddr);
//     console.log("idArray", idArray);
//     // res.status(200).end();
//     var errFlag = false;
//     var tmpPromise = true;
//     i = 6;
//     Cells
//         .update(
//             {cell_address: req.body[i].cell_address},{where:{id: req.body[i].id}}
//         )
//         .then(function(){
//             console.log("update id=" + req.body[i].id + " done");
//             res.status(200).end();
//             // mysqlUpdate(i++, iMax, cells);
//         })
//         .catch(function(err){
//             console.log("update id=" + req.body[i].id + " failed", err);
//             res
//                 .status(500)
//                 .json({error: true, errorText: "Update Failed"})
//         });
//     // Cells
//     //     // .find({
//     //     //     where: {id: idArray}
//     //     // })
//     //     // .then(function (results) {
//     //         Cells
//     //             .update(
//     //                     {cell_address: req.body[i].cell_address},{where:{id:req.body[i].id}}
//     //
//     //   //              // {
//     //                 //     {cell_address: req.body[4].cell_address},
//     //                 //     {where: {id: 4}}
//     //                 // }
//     //
//     //
//     //                 // node_address: req.body[3],
//     //                 // {cell_address: req.body[3].cell_address},
//     //                 // part_id: req.body[3].part_id,
//     //                 // full_weight: req.body[3].full_weight,
//     //                 // min_weight: req.body[3].min_weight,
//     //                 // alarm_weight: req.body[3].alarm_weight
//     //                 // },{fields: ['node_address', 'cell_address', 'part_id', 'full_weight', 'min_weight', 'alarm_weight']}
//     //             )
//     //             .then(function(){
//     //                 console.log("update id=" + req.body[i].id + " done");
//     //                 res.status(200).end();
//     //             })
//     //             .catch(function(err){
//     //                 console.log("update failed", err);
//     //                 res
//     //                     .status(500)
//     //                     .json({error: true, errorText: "Update Failed"})
//     //             });
//     //     // }).catch(function (err) {
//     //     //     console.log("update failed");
//     //     //         res
//     //     //           .status(500)
//     //     //           .json({error: true, errorText: "Update Failed"})
//     //     // });
// };

exports.add = function(req,res){
    console.log("--reached cells database--add");
    console.log(req.body);
    if(req.body.length === undefined){
        console.log("single cell add");
        Cells
            .create({
                node_address: req.body.node_address,
                cell_address: req.body.cell_address,
                part_id: Number(req.body.part_id),
                full_weight: Number(req.body.full_weight),
                min_weight: Number(req.body.min_weight),
                alarm_weight: Number(req.body.alarm_weight)
            })
            .then(function(part){
                console.log("New cell added");
                res.status(201).json(part);
            })
            .catch(function(err){
                console.log("err", err);
                res
                    .status(409)
                    .json({error: true, errorText: "Add cell failed"})
            });

    }else{
        console.log("bulk cells add", req.body.length);
        Cells
            .bulkCreate(
                req.body
            )
            .then(function(part){
                console.log("New cells added");
                res.status(201).json(part);
            })
            .catch(function(err){
                console.log("err", err);
                res
                    .status(409)
                    .json({error: true, errorText: "Add cell failed"})
            });
    }
};
