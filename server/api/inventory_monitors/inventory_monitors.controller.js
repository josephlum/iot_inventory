var Inventory_monitors = require("../../database").Inventory;
var Cells = require("../../database").Cell;

exports.list = function(req,res){
    console.log("reached inventory_monitors database >> list");
    console.log(req.query);
    var whereCondition = {};
    var limit = 60;
    if (req.query.cell_id !== undefined){
        console.log("Query single cell");
        // list history for the given cell_id and number of records
        if ((req.query.dateStart !== undefined) && (req.query.dateStart)){
            whereCondition = {
                cell_id: Number(req.query.cell_id),
                createdAt: {
                    $lt: req.query.dateEnd,
                    $gt: req.query.dateStart
                }
            };
        }else{
            whereCondition = {
                cell_id: Number(req.query.cell_id),
                createdAt: {
                    $lt: new Date(),
                    $gt: new Date(new Date() - 30 * 24 * 60 * 60 * 1000)
                }
            };
        }
        if(req.query.cell_id !== undefined){
            limit = Number(req.query.limit);
        }
        Inventory_monitors
            .findAll({
                where: whereCondition,
                limit: limit
            })
            .then(function (result) {
                if (result) {
                    // console.log(result);
                    res.json(result);
                } else {
                    res.status(400).send(JSON.stringify("Record Not Found"));
                }
            });
    }else{
        console.log("Query all cells");
        // list the latest weight for all cells
        Inventory_monitors.max('id').then(function(maxId){
            console.log("maxId: " + maxId);
            Inventory_monitors
                .findAll({
                    where: {
                        id: {$gt: (maxId - Number(req.query.limit))}
                    }

                })
                .then(function (result) {
                    if (result) {
//                    console.log(result);
                        res.json(result);
                    } else {
                        res.status(400).send(JSON.stringify("Record Not Found"));
                    }
                });

        })
    }
};

exports.delete = function(req,res){
    console.log("--reached inventory_monitor database--delete");
    console.log(req.params);
    Inventory_monitors
        .destroy({
            where: {
                id: req.params.inventoryId
            }
        })
        .then(function(result) {
            console.log("deleted");
            res
                .status(200)
                .json(result)
        })
        .catch(function(err){
            console.log("err", err);
            res
                .status(500)
                .json({error: true})
        })

};

exports.update = function(req,res) {
    console.log("--reached inventory_monotors database--update");
    console.log(req.params);
    Inventory_monitors
        .find({
            where: {
                id: Number(req.params.inventoryId)
            }
        })
        .then(function (response) {
            console.log('DB Response', response);
            var update = {};
            if(req.body.cell_id && req.body.quantity && req.body.weight){
                update = {
                    cell_id: req.body.cell_id,
                    quantity: req.body.quantity,
                    weight: req.body.weight
                }
            }else if(req.body.cell_id && req.body.weight){
                update = {
                    cell_id: req.body.cell_id,
                    weight: req.body.weight
                }
            }else if(req.body.cell_id){
                update = {
                    cell_id: req.body.cell_id
                 }
            }else if(req.body.weight){
                update = {
                    weight: req.body.weight
                }
            }
            response.updateAttributes(update)
                .then(function () {
                console.log("update done");
                res.status(200).end();
            }).catch(function () {
                console.log("update failed");
                res
                    .status(500)
                    .json({error: true, errorText: "Update Failed"})
            });
        })
        .catch(function (err) {
            console.log("err", err);
            res
                .status(500)
                .json({error: true, errorText: "Record not found"})
        });
};

exports.add = function(req,res){
        console.log("--reached inventory_monitors database--add");
        console.log(req.body);
        Inventory_monitors
            .create({
                cell_id: req.body.cell_id,
                quantity: req.body.quantity,
                weight: req.body.weight
            })
            .then(function(part){
                console.log("New inventory_monitor added");
                    res.status(201).json(part);
            })
            .catch(function(err){
                console.log("err", err);
                res
                    .status(409)
                    .json({error: true, errorText: "Add inventory_monitor failed"})
            });
};

// exports.add = function(req,res){
//     console.log("--reached inventory_monitors database--add");
//     console.log(req.body);
//     Inventory_monitors
//         .create({
//             cell_id: Number(req.body.cell_id),
//             quantity: req.body.quantity,
//             weight: req.body.weight
//         })
//         .then(function(part){
//             console.log("New inventory_monitor added");
//             res.status(201).json(part);
//         })
//         .catch(function(err){
//             console.log("err", err);
//             res
//                 .status(409)
//                 .json({error: true, errorText: "Add inventory_monitor failed"})
//         });
// };

exports.bulkAdd = function(req,res){
    console.log("--reached inventory_monitors database--bulkAdd");
    console.log(req.body);
    Inventory_monitors
        .bulkCreate(
            req.body
        )
        .then(function(part){
            console.log("New inventory_monitor added");
            res.status(201).json(part);
        })
        .catch(function(err){
            console.log("err", err);
            res
                .status(409)
                .json({error: true, errorText: "Add inventory_monitor failed"})
        });
};
