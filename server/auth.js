var LocalStrategy = require("passport-local").Strategy;

var bcrypt   = require('bcryptjs');

var User = require("./database").User;
var config = require("./config");

//Setup local strategy
module.exports = function (app, passport) {
    function authenticate(username, password, done) {
        var where = {};
        if (username.search("@") > 0){
            where = {email: username};
        }else{
            where = {employee_id: username};
        }
        User.findOne({
            where: where
        }).then(function(result) {
            console.log("findOne result", result);
            if(!result){
                return done(null, false);
            }else{
                if(bcrypt.compareSync(password , result.password)){
                // if(password == result.password){
                    console.log("bcrypt identical");
                    return done(null, result);
                }else{
                    console.log("bcrypt failed");
                    return done(null, false);
                }
            }
        }).catch(function(err){
            console.log("findOne err", err);
            return done(err, false);
        });
    }

    function verifyCallback(accessToken, refreshToken, profile, done) {
        if(profile.provider === 'google' || profile.provider === 'facebook'|| profile.provider === 'linkedin'){
            id = profile.id;
            email = profile.emails[0].value;
            displayName = profile.displayName;
            provider_type = profile.provider;
            var hashpassword = bcrypt.hashSync('socialpwd', bcrypt.genSaltSync(8), null);
            User.findOrCreate({where: {email: email}, defaults: {username: email , email: email, password: hashpassword, name: displayName}})
                .spread(function(user, created) {
                    console.log(user.get({
                        plain: true
                    }));
                    console.log(created);
                    AuthProvider.findOrCreate({where: {userid: user.id, providerType: provider_type},
                        defaults: {providerId: id, userId: user.id, providerType: provider_type, displayName: displayName}})
                        .spread(function(provider, created) {
                            console.log(provider.get({
                                plain: true
                            }));
                            console.log(created);
                        });
                    done(null, user);
                });
        }else if(profile.provider === 'twitter'){
            id = profile.id;
            twitterUsername = profile.username;
            displayName = profile.displayName;
            provider_type = profile.provider;
            var hashpassword = bcrypt.hashSync('socialpwd', bcrypt.genSaltSync(8), null);
            User.findOrCreate({where: {email: twitterUsername}, defaults: {username: twitterUsername, email: twitterUsername, password: hashpassword , name: displayName}})
                .spread(function(user, created) {
                    console.log(user.get({
                        plain: true
                    }));
                    console.log(created);
                    AuthProvider.findOrCreate({where: {userid: user.id, providerType: provider_type},
                        defaults: {providerId: id, userId: user.id, providerType: provider_type, displayName: displayName}})
                        .spread(function(provider, created) {
                            console.log(provider.get({
                                plain: true
                            }));
                            console.log(created);
                        });
                    done(null, user);
                });
        }else{
            done(null, false);
        }
    }

    passport.use(new LocalStrategy({
        usernameField: "email",
        passwordField: "password"
    }, authenticate));

    passport.serializeUser(function (user, done) {
        // console.info("serial to session");
        done(null, user);
    });

    passport.deserializeUser(function (user, done) {
        // console.info("deserializeUser to session", user);
        User.findOne({
            where: {
                email: user.email
            }
        }).then(function(result) {
            // console.info("deserializeUser to session - result", result);
            if(result){
                done(null, user);
            }
        }).catch(function(err){
            // console.info("deserializeUser to session - err", err);
            done(err, user);
        });
    });
};