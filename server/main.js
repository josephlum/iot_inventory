/**
 * Created by LENOVO on 10/24/2016.
 */
const express = require("express");
const path = require("path");
var bodyParser = require("body-parser");
var cookieParser = require('cookie-parser');
var session = require("express-session");
var passport = require("passport");

var database = require("./database");
var routes = require("./route");
// var iotMailgun = require("/api/iot_mailgun/iot_mailgun.js");

const app = express();
const docroot = path.join(__dirname, "..");

app.set("port", process.argv[2] || process.env.APP_PORT || 3000);

app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false}));
console.log(bodyParser.json);
app.use(bodyParser.json());

// Initialize session
app.use(session({
    secret: "iot-inventory",
    resave: false,
    saveUninitialized: true
}));

//Initialize passport
app.use(passport.initialize());
app.use(passport.session());

//Include auth.js
require("./auth")(app, passport);

app.use(express.static(path.join(docroot, "client")));
app.use(express.static(path.join(docroot, "client/bower_components")));
app.use(express.static(path.join(docroot, "client/api")));
routes.init(app);
routes.errorHandler(app);


app.listen(app.get("port"), function () {
    console.info("App server start to listen at port: %d", app.get("port"));
    console.info("__dirname: " + __dirname);
    console.info("docroot: " + docroot);
    
});
