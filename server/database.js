var Sequelize = require("sequelize");
var config = require('./config');

var database = new Sequelize(
    config.mysql.database,
    config.mysql.username,

    config.mysql.password, {
        host:config.mysql.host,
        dialect:"mysql",
        pool:{
            max: 5,
            min: 0,
            idle:10000
        },
        force:false
    });

var CellModel = require('./models/cells.model.js')(database);
var InventoryMonitorModel = require('./models/inventory_monitors.model.js')(database);
var VendorModel = require('./models/vendors.model.js')(database);
var UserModel = require('./models/users.model.js')(database);
// var PurchaserModel = require('./models/purchasers.model.js')(database);
var PartModel = require('./models/parts.model')(database);

CellModel.hasMany(InventoryMonitorModel, {foreignKey: "fk_cell_id"});
VendorModel.hasMany(PartModel, {foreignKey: "fk_vendor_id"});
UserModel.hasMany(PartModel, {foreignKey: "fk_purchaser_id"});
// VendorModel.hasMany(PurchaserModel, {foreignKey: "fk_purchaser_vendor_id"});
// UserModel.hasMany(PurchaserModel, {foreignKey: "fk_user_id"});
PartModel.hasOne(CellModel, {foreignKey: "fk_part_id"});
// PurchaserModel.hasMany(CellModel, {foreignKey: "fk_purchaser_id"});


database.sync()
    .then(function () {
        console.log("DB in sync")
    });

module.exports = {
    Cell: CellModel,
    Inventory: InventoryMonitorModel,
    Vendor: VendorModel,
    User: UserModel,
    // Purchaser: PurchaserModel,
    Part: PartModel
};