'use strict';

var express = require("express");
var path = require("path");
var passport = require("passport");
var Users = require("./api/users/users.controller");
var Vendors = require("./api/vendors/vendors.controller");
var Parts = require("./api/parts/parts.controller");
var Cells = require("./api/cells/cells.controller");
var Inventory_monitors = require("./api/inventory_monitors/inventory_monitors.controller");

const CLIENT_FOLDER = path.join(__dirname + '/../client');


module.exports = {
    init: configureRoutes,
    errorHandler: errorHandler
};

function configureRoutes(app){
    app.use(express.static(CLIENT_FOLDER));
    // login
    app.post("/login", passport.authenticate("local", {
            successRedirect: "/",
            failureRedirect: "/login",
            failureFlash : true
        }
    ));

    app.get("/status/user", function (req, res) {
        var status = "";
        if(req.user) {
            status = req.user.email;
        }
        console.info("status of the user --> " + status);
        res.send(status).end();
    });

    // logout
    app.get("/logout", function(req, res) {
        req.logout();             // clears the passport session
        req.session.destroy();    // destroys all session related data
        res.send(req.user).end();
    });

    function isAuthenticated(req, res, next) {
        if (req.isAuthenticated())
            return next();
        res.redirect('/');
    }

    // users
    app.get("/api/users", isAuthenticated, Users.list);
    app.delete("/api/users/:userId", Users.delete);
    app.put("/api/users/:userId", Users.update);
    app.post("/api/users/", Users.add);

    // vendors
    app.get("/api/vendors/", Vendors.list);
    app.get("/api/vendors/name/:name", Vendors.listByName);
    app.get("/api/vendors/email/:email", Vendors.listByEmail);
    app.delete("/api/vendors/:vendorId", Vendors.delete);
    app.put("/api/vendors/:vendorId", Vendors.update);
    app.post("/api/vendors/", Vendors.add);

    // parts
    app.get("/api/parts", Parts.list);
    // app.get("/api/parts/part_name/:part_name", Parts.listByPartName);
    // app.get("/api/parts/part_number/:part_number", Parts.listByPartNumber);
    // app.get("/api/parts/mfc_part_number/:mfc_part_number", Parts.listByMfcPartNumber);
    // app.get("/api/parts/vendor_id/:vendor_id", Parts.listByVendorId);
    app.delete("/api/parts/:partId", Parts.delete);
    app.put("/api/parts", Parts.update);
    app.post("/api/parts/", Parts.add);

    // cells
    app.get("/api/cells", isAuthenticated, Cells.list);
    app.delete("/api/cells/:cellId", isAuthenticated, Cells.delete);
    app.put("/api/cells/:cellId", isAuthenticated, Cells.update);
    app.put("/api/cells", isAuthenticated, Cells.bulkUpdate);
    app.post("/api/cells", isAuthenticated, Cells.add);

    // inventory_monitors
    app.get("/api/inventory_monitors", isAuthenticated, Inventory_monitors.list);
    app.delete("/api/inventory_monitors/:inventoryId", Inventory_monitors.delete);
    app.put("/api/inventory_monitors/:inventoryId", Inventory_monitors.update);
    app.post("/api/inventory_monitor", Inventory_monitors.add);
    app.post("/api/inventory_monitors", isAuthenticated, Inventory_monitors.bulkAdd);

    function isAuthenticated(req, res, next) {
        if (req.isAuthenticated())
            return next();
        res.redirect('/');
    }
}


function errorHandler(app) {
    app.use(function (req, res) {
        res.status(401).sendFile(CLIENT_FOLDER + "/app/errors/404.html");
    });

    app.use(function (err, req, res, next) {
        res.status(500).sendFile(path.join(CLIENT_FOLDER + '/app/errors/500.html'));
    });
};
